document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", () => {
    jsConsole.writeLine(cutString(document.getElementById("text").value));
    jsConsole.delimiter();
})

function cutString(str, length = 20)
{
    if(typeof str !== "string") return;
    return str.slice(0, length) + (str.length > length? "..." : "");
}
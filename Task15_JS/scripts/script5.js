document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", () => {
    jsConsole.writeLine(checkSpam(document.getElementById("text").value));
})

const spamWords = ["spam", "sex"];

function checkSpam(str)
{
    if(typeof str !== "string") return;
    str = str.toLowerCase();
    for(w of spamWords)
    {
        if(str.includes(w)) return true;
    }
    return false
}
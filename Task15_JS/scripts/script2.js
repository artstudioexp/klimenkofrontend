document.getElementById("clear").addEventListener("click", jsConsole.clear);

document.getElementById("execute").addEventListener("click", function() {
    jsConsole.writeLine(compare(document.getElementById("a-param").value, document.getElementById("b-param").value));
    jsConsole.delimiter();
});

const compare = function comp(a, b) {
    return a == b ? 1 : -1;
}
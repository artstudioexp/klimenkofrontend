const fib = function fibonacci(n) {
    n = Number(n);
    if(n <= 0) return undefined;
    if(n === 1 || n === 2) return 1;
    let prevPrev = 1;
    let prev = 1;
    let temp = 0;
    for(let i = 2; i < n; ++i)
    {
        temp = prevPrev;
        prevPrev = prev;
        prev += temp;
    }
    return prev;
}

const exec = function execute() {
    jsConsole.writeLine(fib(document.getElementById("param").value));
    jsConsole.delimiter();
}

document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", exec);


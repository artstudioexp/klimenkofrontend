document.getElementById("clear").addEventListener("click", jsConsole.clear);

document.getElementById("execute").addEventListener("click", function() {
    jsConsole.writeLine(conc(document.getElementById('a-param').value, document.getElementById('b-param').value));
    jsConsole.delimiter();
})

function conc(...args)
{
    let str = "";
    for(el of args)
    {
        str += el;
    }
    return str;
}
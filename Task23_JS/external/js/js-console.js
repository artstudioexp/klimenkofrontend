﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text, color) {
			var textLine = document.createElement("span");
			if(color) textLine.style.color = color;
			textLine.style.color = color;
			textLine.innerText = text;
			textArea.appendChild(textLine);
			//consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeHTML = function jsConsoleWrite(text, color) {
			var textLine = document.createElement("span");
			if(color) textLine.style.color = color;
			textLine.style.color = color;
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			//consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text = "", color) {
			self.write(text, color);
			textArea.appendChild(document.createElement("br"));
		}

		self.writeLineHTML = function jsConsoleWriteLine(text = "", color) {
			self.writeHTML(text, color);
			textArea.appendChild(document.createElement("br"));
		}


		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		// clear console content
		self.clear = function jsConsoleClear() {
			textArea.innerHTML = "";
		}

		// draw sequence of delimiting characters
		self.delimiter = function jsConsoleDelimiter(count = 30, char = "-", color) {
			let newLine = char;
			this.writeLine(newLine.repeat(count), color);
		}

		self.indent = function createIndentation(count = 1) {
    		for(let i = 0; i < count; ++i) this.writeHTML("&nbsp&nbsp&nbsp");
		}
		
		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

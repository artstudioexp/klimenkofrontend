let container;
let header;
let addButton;
let hideButton;
let showButton;
let removeButton;
let list;
let input;
let elements;

initialize();

function addElement()
{
    if(!input.value) return;
    let element = createLi(input.value);
    elements.push(element);
    input.value = "";
    list.appendChild(element);
}

function hideSelected()
{
    for(el of elements)
    {
        let check = el.getElementsByTagName("input")[0];
        if(check.checked) el.style.display = "none";
    }
}

function showAll()
{
    for(el of elements)
    {
        let check = el.getElementsByTagName("input")[0];
        if(el.style.display === "none") el.style.display = "list-item";
    }
}

function removeSelected()
{
    for(el of elements)
    {
        let check = el.getElementsByTagName("input")[0];
        if(check.checked && el.style.display !== "none") el.remove();
    }
    elements = elements.filter(el => document.body.contains(el));
}

function initialize()
{
    container = document.getElementById("container");
    header = document.createElement("p");
    list = document.createElement("ul");
    list.style.listStyleType = "none";
    header.innerText = "Elements:";
    input = document.createElement("input");
    input.type = "text";
    addButton = document.createElement("button");
    addButton.addEventListener("click", addElement);
    addButton.innerText = "Add";
    hideButton = document.createElement("button");
    hideButton.addEventListener("click", hideSelected);
    hideButton.innerText = "Hide selected";
    showButton = document.createElement("button");
    showButton.addEventListener("click", showAll);
    showButton.innerText = "Show hidden";
    removeButton = document.createElement("button");
    removeButton.addEventListener("click", removeSelected);
    removeButton.innerText = "Remove selected";

    container.appendChild(header);
    container.appendChild(list);
    container.appendChild(input);
    container.appendChild(addButton);
    container.appendChild(removeButton);
    container.appendChild(hideButton);
    container.appendChild(showButton);

    elements = [];

    // DEFAULT VALUES
    elements.push(list.appendChild(createLi("Александр Александрович")));
    elements.push(list.appendChild(createLi("Евгений Евгеньевич")));
    elements.push(list.appendChild(createLi("Андрей Андреевич")));
    elements.push(list.appendChild(createLi("Сергей Сергеевич")));
    //
}

function createLi(str)
{
    let check = document.createElement("input");
    check.type = "checkbox";
    let span = document.createElement("span");
    span.innerText = str;
    span.style.margin = "7px";
    span.addEventListener("click", () => { check.checked = !check.checked; });
    let li = document.createElement("li");
    li.appendChild(check);
    li.appendChild(span);
    return li;
}
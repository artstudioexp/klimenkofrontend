let input = document.getElementById("input");
let jsConsoleElement = document.getElementById("console");

let number;
let numberOfTries = 0;
initialize();


function initialize()
{
    document.getElementById("btn").addEventListener("click", guess);
    generateNumber();
}

function generateNumber()
{
    number = [];
    number[0] = Math.ceil(Math.random() * 8) + 1;
    for(let i = 1; i < 4; ++i)
    {
        let randNumber;
        do
        {
            randNumber = Math.round(Math.random() * 9);
        } while (number.includes(randNumber))
        number[i] = randNumber;
    }
    console.log(number);
}

function guess() 
{
    let guessNumber = readNumber();
    let error = checkNumberForError(guessNumber);
    if(error)
    {
        jsConsole.writeLine(error, "red");
        return;
    }
    
    let rams = sheeps = 0;

    // check for rams/sheeps
    for(let i = 0; i < 4; ++i)
    {
        if(guessNumber[i] === number[i]) rams++;
        else
        {
            for(let j = 0; j < 4; ++j)
            {
                if(guessNumber[i] === number[j] && guessNumber[j] !== number[j]) 
                {
                    sheeps++;
                    break;
                }
            }
        }
    }

    numberOfTries++;

    jsConsole.writeLine(rams + (rams === 1? " ram, " : " rams, ") + sheeps + (sheeps === 1? " sheep." : " sheeps."));

    // player won
    if(rams === 4)
    {
        jsConsole.delimiter(50, "*", "aquamarine");
        jsConsole.writeLine("You've won!");
        spawnUsernameInput();
    }
}

function readNumber()
{
    let number = [];
    for(i in input.value)
    {
        number[i] = Number(input.value[i]);
    }
    return number;
}

function checkNumberForError(guessNumber)
{
    if(guessNumber.length !== 4) 
    {
        return "Invalid number. 4 digits are required!";
    }
    else if(guessNumber[0] <= 0) 
    {
        return "Invalid number. First digit's value must be greater than zero!";
    }
    else
    {
        return "";
    }
}

function spawnUsernameInput()
{
    spawnUsernameInput.nameInput = document.createElement("input");
    spawnUsernameInput.nameInput.type = "text";
    spawnUsernameInput.nameInput.name = "name-input";
    spawnUsernameInput.nameInput.id = "name-input";

    spawnUsernameInput.label = document.createElement("label");
    spawnUsernameInput.label.for = "name-input";
    spawnUsernameInput.label.innerText = "Enter your name: ";

    spawnUsernameInput.submitButton = document.createElement("button");
    spawnUsernameInput.submitButton.innerText = "Submit";
    spawnUsernameInput.submitButton.addEventListener("click", () => { submitUserName(spawnUsernameInput.nameInput.value)});

    jsConsoleElement.appendChild(spawnUsernameInput.label);
    jsConsoleElement.appendChild( spawnUsernameInput.nameInput);
    jsConsoleElement.appendChild( spawnUsernameInput.submitButton);
}

function submitUserName(name)
{
    localStorage.setItem(name, numberOfTries);

    spawnUsernameInput.nameInput.remove();
    spawnUsernameInput.label.remove();
    spawnUsernameInput.submitButton.remove();

    jsConsole.delimiter(40, "-", "lightgreen");
    jsConsole.writeLine("User results:", "lightblue");
    for(let i = 0; i < localStorage.length; ++i)
    {
        let username = localStorage.key(i)
        let tries = localStorage.getItem(username);
        jsConsole.writeLine(username + " : " + tries + (tries == "1"? " try" : " tries"), "lightgreen");
    }

    jsConsole.delimiter(40, "-", "lightgreen");

    let againButton = document.createElement("button");
    againButton.innerText = "Play again";
    againButton.addEventListener("click", (e) => { 
        numberOfTries = 0;
        generateNumber();
        jsConsole.clear();
        e.target.remove();
     })
    jsConsoleElement.appendChild(againButton);
}
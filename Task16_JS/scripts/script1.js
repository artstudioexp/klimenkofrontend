document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);

// main execution function
function execute() {
    let firstName = document.getElementById("first-name").value;
    if(!firstName)
    {
        jsConsole.writeLine("First name field is empty")
        jsConsole.delimiter();
        return;
    }
    let lastName = document.getElementById("last-name").value
    if(!lastName)
    {
        jsConsole.writeLine("Last name field is empty")
        jsConsole.delimiter();
        return;
    }
    let age = document.getElementById("age").value
    if(!age)
    {
        jsConsole.writeLine("Age field is empty")
        jsConsole.delimiter();
        return;
    }
    let person = new Person(firstName, lastName, age);
    let line = person.introduce();
    if(line)
    {
        jsConsole.writeLine(line);
        jsConsole.delimiter();
    }
}

// person constructor function
function Person(firstName, lastName, age)
{
    this.fullname = firstName + " " + lastName;
    this.age = age;
}

// add introduce() function to person's prototype
Person.prototype.introduce = function() {
    if((this.age || this.age === 0) && this.firstName && this.lastName)
        return "Hello! My name is " + this.fullname + " and I am " + this.age + "-years-old";
    else
        return "";
}

// add getter/setter functions to person's prototype
Object.defineProperties(Person.prototype, {
    "fullname" : {
        "get" : function() {
            return this.firstName + " " + this.lastName;
        },
        "set": function(val) {
            let nameArray = val.split(" ");
            if(nameIsValid(nameArray[0]) && nameIsValid(nameArray[1]))
            {
                this.firstName = nameArray[0];
                this.lastName = nameArray[1];
            }
            else
            {
                jsConsole.writeLine("Name is incorrect!");
                jsConsole.delimiter();
                return;
            }

        }
    },
    "age" : {
        "get": function() {
            return Number(this._age);
        },
        "set": function(val) {
            ///// конвертируемое в число значение
            val = stringToNumber(val);
            ///
            if(ageIsValid(val))
            {
                this._age = Number(val);
            }
            else
            {
                jsConsole.writeLine("Age is incorrect!");
                jsConsole.delimiter();
                return;
            }
        }
    }
})

// check if the name is valid
function nameIsValid(str) {
    let l = str.length;
    return (str.length >= 3 && str.length <= 20 && /^[\x40-\x7F]*$/.test(str));
}

// check if the age is valid
function ageIsValid(value)
{
    return Number(value) >= 0 && Number(value) <= 150;
}

// convert particular string values to numbers
function stringToNumber(str)
{
    newStr = str.toLowerCase();
    switch(newStr)
    {
        case "true":
            return 1;
            break;
        case "false":
        case "null":
            return 0;
            break;
        default:
            return str;
            break;
    }
}
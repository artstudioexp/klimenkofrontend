document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);
document.getElementById("str").value = "The coordinates of the {0} are X:{1} and Y: {2}";
document.getElementById("args").value = '"rectangle", 25, 36';

function execute() 
{
    let args = document.getElementById("args").value.split(",");
    jsConsole.writeLine(stringFormat(document.getElementById("str").value, args));
    jsConsole.delimiter(50, '-', "aquamarine");
}

function stringFormat(str, args)
{
    let match = str.match(/\{\d\}/);
    while(match)
    {
        str = str.splice(match.index, 3, args[match[0][1]]);
        console.log(str);
        match = str.match(/\{\d\}/);
    }
    return str;
}

String.prototype.splice = function(start, deleteCount, replace) {
    return this.slice(0, start) + replace + this.slice(start + deleteCount);
}
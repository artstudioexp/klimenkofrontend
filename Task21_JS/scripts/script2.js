document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);

let people = [
    { name: "Шапокляк", age: 55 },
    { name: "Чебурашка", age: 17 },
    { name: "Крыска-Лариска", age: 18 },
    { name: "Крокодильчик", age: 26 },
    { name: "Турист- завтрак крокодильчика", age: 32 },
]

let template = `<strong>-{name}-</strong> <span>-{age}-</span>`;

function execute()
{
    let list = document.createElement("ul");
    for(i in people)
    {
        let li = document.createElement("li");
        li.innerHTML = formatString(template, people[i]);
        list.appendChild(li);
    }
    jsConsole.writeHTML(list.innerHTML);
    jsConsole.delimiter(50, "-", "aquamarine");
}

function formatString(str, args)
{
    let result;
    for(i in args)
    {
        console.log(str);
        result = str.replace(/\{name\}/, args.name);
        result = result.replace(/\{age\}/, args.age);
    }
    return result;
}
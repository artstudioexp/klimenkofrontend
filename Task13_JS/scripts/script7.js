
// function from the previous part (part 6)
function isBiggerThanNeighbours(idx, array)
{
    return +array[idx] > +array[idx - 1] && +array[idx] > +array[idx + 1];
}

function findFirstIndex(array){
    let idx = -1;
    for(i in array)
    {
        if(isBiggerThanNeighbours(+i, array))
        {
            idx = i;
            break;
        }
    }
    return idx;
}

function execute() {
    const array = document.getElementById("sequence").value.split(',');
    jsConsole.writeLine("Element index: " + findFirstIndex(array));
    jsConsole.delimiter();
}

document.getElementById("execute").addEventListener("click", execute);
document.getElementById("clear").addEventListener("click", jsConsole.clear);
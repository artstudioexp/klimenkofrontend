function findOccurrences()
{
    let sequence = document.getElementById("sequence").value.split(",");
    let number = document.getElementById("number").value;
    let count = 0;
    for(num of sequence)
    {
        if(num == number) count++;
    }
    jsConsole.writeLine("Number " + number + " occurres " + count + " time" + (count == 1? " " : "s ") + "in the sequence");
    jsConsole.delimiter();
}

function test()
{
    let quantity = Math.floor(Math.random() * 20.0);
    let array = [];
    for(let i = 0; i < quantity; ++i)
    {
        array.push(7);
    }
    document.getElementById("sequence").value = array.toString();
    document.getElementById("number").value = 7;
    document.getElementById("execute").click();
}

document.getElementById("execute").addEventListener("click", findOccurrences)
document.getElementById("test").addEventListener("click", test)
document.getElementById("clear").addEventListener("click", jsConsole.clear);
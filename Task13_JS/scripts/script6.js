function isBiggerThanNeighbours(idx, array)
{
    return +array[idx] > +array[idx - 1] && +array[idx] > +array[idx + 1];
}

function execute()
{
    const array = document.getElementById("sequence").value.split(",");
    const position = Number(document.getElementById("position").value);

    if(position === 0)
    {
        jsConsole.writeLine("First element of the sequence is forbidden to use"); 
        return;
    } 
    if(position === array.length - 1)
    {
        jsConsole.writeLine("Last element of the sequence is forbidden to use"); 
        return;
    } 
    if(position < 0 || position >= array.length)
    {
        jsConsole.writeLine("Out of bounds sequence position"); 
        return;
    } 

    jsConsole.writeLine(isBiggerThanNeighbours(position, array));
}

document.getElementById("execute").addEventListener("click", execute);
document.getElementById("clear").addEventListener("click", jsConsole.clear);
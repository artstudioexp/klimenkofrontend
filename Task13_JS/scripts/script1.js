let strings = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

document.getElementById("execute").addEventListener("click", function() {
    let number = Number(document.getElementById("number").value.substr(-1));
    jsConsole.writeLine(strings[number]);
})
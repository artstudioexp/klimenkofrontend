const divGet = document.getElementById("div-get");
const divPost = document.getElementById("div-post");
const form = document.getElementById("form");

form.style.display = "none";

document.getElementById("get").addEventListener("click", function(e) {
    getJSON().then(function(response) {
        divGet.innerHTML = response;
    }, function(error) {
        divGet.innerHTML = error.status + " " + error.statusText;
    }); 
});

document.getElementById("post").addEventListener("click", function(e) {
    if(form.style.display == "none")
    {
        form.style.display = "inline";
    }
    else
    {
        form.style.display = "none";
        let formdata = new FormData(form);

        let message = {
            firstName: formdata.get("first-name"),
            lastName: formdata.get("last-name"),
            age: formdata.get("age")
        }

        postJSON(JSON.stringify(message)).then(function(response) {
            divPost.innerHTML = response;
        }, function(error) {
            divPost.innerHTML = error.status + " " + error.statusText;
        });
    }
});

function getJSON()
{
    return new Promise(function(resolve, reject) {
        let request = new XMLHttpRequest();
        request.open("GET", "01.data.json");
        request.onload = function() {
            if(this.status >= 200 && this.status < 300) 
            {
                resolve(request.response);
            }
            else
            {
                reject({
                    status: this.status,
                    statusText: this.statusText
                })
            }
        };
        request.onerror = function() {
            reject({
                status: this.status,
                statusText: this.statusText
            })
        };
        request.send();
    });
}

function postJSON(json)
{
    return new Promise(function(resolve, reject) {
        let request = new XMLHttpRequest();
        request.open("POST", "submit.php");
        request.setRequestHeader("Conent-type", "application/json");

        request.onload = function() {
            if(this.status >= 200 && this.status < 300) 
            {
                resolve(request.responseText);
            }
            else
            {
                reject({
                    status: this.status,
                    statusText: this.statusText
                })
            }
        };
        request.onerror = function() {
            reject({
                status: this.status,
                statusText: this.statusText
            })
        };
        request.send(json);
    });
}
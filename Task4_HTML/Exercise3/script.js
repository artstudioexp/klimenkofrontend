let tr = document.getElementsByClassName("table_row")[0];  // get template row
let itemReport = document.getElementById("item_report");
let pagesizeBtn = document.getElementById("page_size");
let pageSelect = document.getElementById("page_select")
let allPrevBtn = document.getElementById("all_prev");
let prevBtn = document.getElementById("prev");
let allNextBtn = document.getElementById("all_next");
let nextBtn = document.getElementById("next");

const numberOfRecords = 30;     // maximum number of table records
let numberOfPages = 3;
let currentPage = 1;
let currentPageSize = 10;

pagesizeBtn.addEventListener("click", refreshPage);
allPrevBtn.addEventListener("click", function() { changePage(1); })
prevBtn.addEventListener("click", function() { changePage(currentPage - 1); })
allNextBtn.addEventListener("click", function() { changePage(numberOfPages); })
nextBtn.addEventListener("click", function() { changePage(currentPage + 1); })


tr.hidden = true;   // hide template row
refreshPage();


function refreshPage()
{
    clearTable();

    let newPageSize = Number(pagesizeBtn.value);

    tr.children[0].innerHTML = 1;

    numberOfPages = numberOfRecords / newPageSize;

    if(currentPage > numberOfPages)
    {
        currentPage = numberOfPages;
    }

    for(let i = newPageSize; i > 0; --i)
    {
        let newElement = tr.cloneNode(true);
        newElement.children[0].innerHTML = i + newPageSize * (currentPage - 1);
        //newElement.children[2].innerHTML = '$' + Math.floor(Math.random() * 99) + ".00";
        tr.insertAdjacentHTML("afterend", newElement.innerHTML);
    }

    currentPageSize = newPageSize;

    renderPageSelect();
    itemReport.innerText = numberOfRecords + " items in " + numberOfPages + " pages";
}

function changePage(pageNumber)
{
    if(pageNumber === currentPage || pageNumber < 1 || pageNumber > numberOfPages) return;
    currentPage = pageNumber;
    refreshPage();
}

function renderPageSelect()
{
    pageSelect.innerHTML = "";
    for(let i = 0; i < numberOfPages; ++i)
    {
        let a = document.createElement("a");
        a.href = "#";
        a.innerText = i + 1;
        a.addEventListener("click", function(event){changePage(Number(event.target.innerText))});
        pageSelect.appendChild(a);
        pageSelect.append(', ');

        if(i === currentPage - 1)
        {
            a.style.color = "green";
            a.style.textDecoration = "none";
        }
    }
}

function clearTable()
{
    for(let sibling = tr.nextElementSibling; sibling != null; sibling = tr.nextElementSibling)
    {
        sibling.remove();
    }
}
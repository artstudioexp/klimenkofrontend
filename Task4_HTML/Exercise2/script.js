let prevButton = document.getElementById("prev");
let nextButton = document.getElementById("next");

prevButton.addEventListener("click", navSubmit);
nextButton.addEventListener("click", navSubmit);

function navSubmit(event)
{
    let path = window.location.pathname;
    let page = path.split("/").pop();
    let number = Number(page.slice(5, 6));

    if(event.target.id === "next")
    {
        if(number < 3)
        {
            number += 1
            document.location.href = "./table" + number + ".html";
            console.log(document.location.href);
        }
    }
    else
    {
        if(number > 1)
        {
            number -= 1
            document.location.href = "./table" + number + ".html";
        }
    }
}
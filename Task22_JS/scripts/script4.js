document.getElementById("draw-interval").addEventListener("input", (event) => {
    document.getElementById("draw-interval-label").innerText = "Draw interval (ms): " + event.target.value;
    if(moveDivs.interval) clearInterval(moveDivs.interval);
    moveDivs.interval = setInterval(moveDivs, event.target.value);
});

document.getElementById("quantity").addEventListener("input", (event) => {
    document.getElementById("quantity-label").innerText = "Quantity: " + event.target.value;
    quantity = event.target.value;
    createDivs();
});

document.getElementById("speed").addEventListener("input", (event) => {
    document.getElementById("speed-label").innerText = "Speed (deg/s): " + event.target.value;
    speed = event.target.value * (Math.PI / 180.0);
});

document.getElementById("radius").addEventListener("input", (event) => {
    document.getElementById("radius-label").innerText = "Radius (px): " + event.target.value;
    radius = event.target.value;
});

let divs = [];
let centerX = 400;
let centerY = 300;
let radius = 0;
let speed = 0;
let quantity = 0;

const startEvent = new Event("input");
document.getElementById("draw-interval").dispatchEvent(startEvent);
document.getElementById("quantity").dispatchEvent(startEvent);
document.getElementById("speed").dispatchEvent(startEvent);
document.getElementById("radius").dispatchEvent(startEvent);


function createDivs()
{
    divs.forEach((el) => { el.remove() });
    divs = [];
    let jsConsole = document.getElementById("console");
    let quantity = document.getElementById("quantity").value;
    let phi = (2.0*Math.PI) / quantity;
    for(let i = 0; i < quantity; ++i)
    {
        let el = document.createElement("div");
        el.classList.add("circle");
        divs.push(el);
        let angle = phi * i;
        el.style.transform = `translate(${radius * Math.cos(angle) + centerX}px,${radius * Math.sin(angle) + centerY}px)`;
        jsConsole.appendChild(el);
    }
}

function moveDivs()
{
    if(divs.length == 0) return;
    let radius = document.getElementById("radius").value;
    let phi = (2.0*Math.PI) / quantity;
    let time = (new Date()).getTime();
    for(let i = 0; i < quantity; ++i)
    {
        let angle = phi * i + time * 0.001 * speed;
        divs[i].style.transform = `translate(${radius * Math.cos(angle) + centerX}px,${radius * Math.sin(angle) + centerY}px)`;
    }
}

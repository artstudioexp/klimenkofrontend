document.getElementById("tagname").addEventListener("click", selectByTagName);
document.getElementById("selector").addEventListener("click", selectBySelector);

function selectByTagName()
{
    let divs = document.body.getElementsByTagName("div");
    for(d of divs)
    {
        if(d.parentElement.tagName == "DIV")
        {
            d.style.borderColor = "red";
        }
    }
}

function selectBySelector()
{
    let divs = document.body.querySelectorAll("div div")
    for(d of divs)
    {
        d.style.borderColor = "green";
    }
}
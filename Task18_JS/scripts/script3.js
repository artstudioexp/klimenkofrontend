document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);

var persons = [
    { firstname: "Natalya", lastname: "Osipenko", age: 61 },
    { firstname: "Kristina", lastname: "Osipenko", age:23 },
    { firstname: "Artem", lastname: "Korhov", age: 25 },
    { firstname: "Artem", lastname: "Seredinskiy", age: 20 },
    { firstname: "Artem", lastname: "Artsiomenka", age: 20 },
    { firstname: "Sergey", lastname: "Osipenko", age: 20 },
    { firstname: "Vinni", lastname: "Puh", age: 15 }
];

function groupPeople(array, property)
{
    let list = {};
    for(p of array)
    {
        if(!p.hasOwnProperty(property)) continue;

        let key = p[property];
        if(list.hasOwnProperty(key))
        {
            list[key].push(p);
        }
        else 
        {
            list[key] = []; 
            list[key].push(p);
        }
    }
    return list;
}

function execute()
{
    let list = groupPeople(persons, "age");
    jsConsole.writeLine("Grouped by age:", "aquamarine");
    for(k in list)
    {
        jsConsole.write(k);
        jsConsole.writeLine(' (length=' + list[k].length + ')', "lightblue")
    }
    jsConsole.delimiter(60, '-', "lightgreen");

    list = groupPeople(persons, "lastname")
    jsConsole.writeLine("Grouped by lastname:", "aquamarine");
    for(k in list)
    {
        jsConsole.write(k);
        jsConsole.writeLine(' (length=' + list[k].length + ')', "lightblue")
    }
    jsConsole.delimiter(60, '-', "lightgreen");

    list = groupPeople(persons, "firstname")
    jsConsole.writeLine("Grouped by firstname:", "aquamarine");
    for(k in list)
    {
        jsConsole.write(k);
        jsConsole.writeLine(' (length=' + list[k].length + ')', "lightblue")
    }
    jsConsole.delimiter(60, '-', "lightgreen");
}
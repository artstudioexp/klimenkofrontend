document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);

function Person(firstName, lastName, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;

}

Person.prototype.toString = function() {
    return this.firstName + " " + this.lastName + " - " + this.age;
}

let persons = [
    new Person("Gosho", "Petrov", 32),
    new Person("Bay", "Ivan", 81),
    new Person("Alexey", "Ivanov", 22),
    new Person("Paul", "McGregor", 54),
    new Person("John", "Johnson", 54)
]

function execute()
{
    console.log(persons[0]);
    let youngest = persons[0];
    for(p of persons)
    {
        if(p.age < youngest.age) youngest = p;
    }
    jsConsole.writeLine("The youngest Person is:");
    jsConsole.writeLine(youngest, "lightgreen");
}
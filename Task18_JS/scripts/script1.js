document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);

let obj = {
    "name": "John",
    "age": 23,
    "height": 177,
    "weight": 78
}

function execute()
{
    let prop = document.getElementById("property").value;
    if(obj.hasOwnProperty(prop)) jsConsole.writeLine("Object HAS own property \"" + prop + "\".");
    else jsConsole.writeLine("Object DOESN'T HAVE property \"" + prop + "\".", "red");
}
document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("print-array-btn").addEventListener("click", printArray);
document.getElementById("remove-btn").addEventListener("click", removeElement);
document.getElementById("write-btn").addEventListener("click", writeArray);

let array;

Array.prototype.remove = function(val) {
    let valueType = typeof val;
    return array.filter(element => element != val);
}

function removeElement()
{
    let element =  document.getElementById("remove-value").value;
    array = array.remove(element);
    jsConsole.writeLine("Element [" + element + "] was removed from array.");
}

function printArray()
{
    jsConsole.writeLine("Array: [" + array + "]");
}

function writeArray() {
    array = document.getElementById("array").value.split(",");
    printArray();
    jsConsole.writeLine("was written into memory");
}
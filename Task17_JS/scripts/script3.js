document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);


function execute()
{
    let obj = {
        "name": "John",
        "age": 28,
        "introduce": function() { console.log(this.name);},
        "favorite stuff" : {
            "fruit": "apple",
            "color": "red"
        }
    }

    jsConsole.writeLine("Initial object:", "red");
    printObject(obj);
    jsConsole.delimiter(50);

    let copiedObj = deepCopy(obj);
    jsConsole.writeLine("Deep copied object:", "red");
    printObject(copiedObj);

    // changing copied object properties
    copiedObj.name = "Felix"
    copiedObj.age = 22;
    copiedObj["favorite stuff"].fruit = "plum";
    copiedObj["favorite stuff"].color = "violet";

    jsConsole.writeLine();
    jsConsole.delimiter(50, '=', "aquamarine");
    jsConsole.writeLine("Deep copied object was changed", "aquamarine")
    jsConsole.delimiter(50, '=', "aquamarine");
    jsConsole.writeLine();


    jsConsole.writeLine("Initial object after change:", "red");
    printObject(obj);

    jsConsole.delimiter(50);

    jsConsole.writeLine("Deep copied object after change:", "red");
    printObject(copiedObj);
    jsConsole.delimiter(50);
}

function deepCopy(obj)
{
    let newObj = {};
    for(const [key, value] of Object.entries(obj))
    {
        switch(typeof value)
        {
            case "bigint":
            case "boolean":
            case "undefined":
            case "number":
                newObj[key] = value;
                break;
            case "string":
                newObj[key] = `${value}`;
                break;
            case "object":
                newObj[key] = deepCopy(value);
                break;
            case "function":
                // printing function, copied by line written below, gives a "[native code]"
                // instead of a function body, but function works as expected if called
                newObj[key] = value.bind(newObj);
                break;
        }
    }
    return newObj;
}

function printObject(obj, indentCount = 0)
{
    jsConsole.indent(indentCount);
    jsConsole.writeLine("{");
    for(const [key, value] of Object.entries(obj))
    {
        jsConsole.indent(indentCount + 1);
        if(typeof value == "object")
        {
            jsConsole.writeLine(`${key}:`, "lightgreen");
            printObject(value, indentCount + 1);
        }
        else
        {
            jsConsole.write(`${key}: `, "lightgreen");
            jsConsole.writeLine(`${value},`);
        }
    }
    jsConsole.indent(indentCount);
    jsConsole.writeLine("}");
}
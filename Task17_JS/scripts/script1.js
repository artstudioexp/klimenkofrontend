document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);
document.getElementById("valid-parameters").addEventListener("click", function() { fillInputsWithValues(true); });
document.getElementById("invalid-parameters").addEventListener("click", function() { fillInputsWithValues(false); });

function execute()
{
    let line1A = new Point(document.getElementById("line1-a-x").value, document.getElementById("line1-a-y").value);
    let line1B = new Point(document.getElementById("line1-b-x").value, document.getElementById("line1-b-y").value);
    let line2A = new Point(document.getElementById("line2-a-x").value, document.getElementById("line2-a-y").value);
    let line2B = new Point(document.getElementById("line2-b-x").value, document.getElementById("line2-b-y").value);
    let line3A = new Point(document.getElementById("line3-a-x").value, document.getElementById("line3-a-y").value);
    let line3B = new Point(document.getElementById("line3-b-x").value, document.getElementById("line3-b-y").value);

    let line1 = new Line(line1A, line1B);
    let line2 = new Line(line2A, line2B);
    let line3 = new Line(line3A, line3B);

    if(canBeTriangle(line1, line2, line3)) jsConsole.writeLine("Given lines CAN create triangle!");
    else jsConsole.writeLine("Given lines CANNOT create triangle!")

    jsConsole.writeLine("");

    jsConsole.writeLine("Line 1:");
    jsConsole.writeLine("Point a" + line1A);
    jsConsole.writeLine("Point b" + line1B);
    jsConsole.writeLine("");
    jsConsole.writeLine("Line 2:");
    jsConsole.writeLine("Point a" + line2A);
    jsConsole.writeLine("Point b" + line2B);
    jsConsole.writeLine("");
    jsConsole.writeLine("Line 3:");
    jsConsole.writeLine("Point a" + line3A);
    jsConsole.writeLine("Point b" + line3B);

    jsConsole.delimiter();
}

function createPoint(x, y)
{
    return new Point(x, y);
}

function createLine(a, b)
{
    return new Line(a, b);
}

function calcDistance(a, b)
{
    if(!a instanceof Point || !b instanceof Point) return undefined;
    return (new Line(a, b)).length();
}

function fillInputsWithValues(parametersAreValid)
{
    if(parametersAreValid)
    {
        document.getElementById("line1-a-x").value = '3';
        document.getElementById("line1-a-y").value = '3';
        document.getElementById("line1-b-x").value = '5';
        document.getElementById("line1-b-y").value = '5';

        document.getElementById("line2-a-x").value = '2';
        document.getElementById("line2-a-y").value = '2';
        document.getElementById("line2-b-x").value = '4';
        document.getElementById("line2-b-y").value = '4';

        document.getElementById("line3-a-x").value = '1';
        document.getElementById("line3-a-y").value = '1';
        document.getElementById("line3-b-x").value = '3';
        document.getElementById("line3-b-y").value = '3';
    }
    else
    {
        document.getElementById("line1-a-x").value = '1';
        document.getElementById("line1-a-y").value = '1';
        document.getElementById("line1-b-x").value = '5';
        document.getElementById("line1-b-y").value = '5';

        document.getElementById("line2-a-x").value = '2';
        document.getElementById("line2-a-y").value = '2';
        document.getElementById("line2-b-x").value = '4';
        document.getElementById("line2-b-y").value = '4';

        document.getElementById("line3-a-x").value = '1';
        document.getElementById("line3-a-y").value = '1';
        document.getElementById("line3-b-x").value = '3';
        document.getElementById("line3-b-y").value = '3';
    }
}

function canBeTriangle(l1, l2, l3)
{
    if(!l1 instanceof Line || !l2 instanceof Line || !l3 instanceof Line) return undefined;
    let ll1 = calcDistance(l1.a, l1.b);
    let ll2 = calcDistance(l2.a, l2.b);
    let ll3 = calcDistance(l3.a, l3.b);
    return (ll1 + ll2 > ll3 && ll1 + ll3 > ll2 && ll3 + ll2 > ll1);
}

///// classes /////
function Point(x, y)
{
    this.x = Number(x);
    this.y = Number(y);
}
Point.prototype.add = function(rhs) {
    if(!rhs instanceof Point) return undefined;
    return new Point(this.x + rhs.x, this.y + rhs.y);
}
Point.prototype.subtract = function(rhs) {
    if(!rhs instanceof Point) return undefined;
    return new Point(this.x - rhs.x, this.y - rhs.y);
}
Point.prototype.toString = function() {
    return '(' + this.x + ',' + this.y + ')'; 
}

function Line(a, b)
{
    this.a = a;
    this.b = b;
}
Line.prototype.length = function() {
    let lineVec = this.b.subtract(this.a);
    return length = Math.sqrt(lineVec.x * lineVec.x + lineVec.y * lineVec.y);
}
///////////////////
///// Task 1 /////

let A = document.getElementById("A");
let B = document.getElementById("B");
let sum = document.getElementById("sum");
let oddNumbers = document.getElementById("odd-numbers")

document.getElementById("calculate-range").addEventListener("click", function() {
    let result = 0;
    oddNumbers.value = "";
    // check if A value is odd
    aisOdd = Number(A.value) % 2 !== 0;
    for(let i = Number(A.value); i <= Number(B.value); ++i)
    {
        result += Number(i);

        if(aisOdd)
        {
            oddNumbers.value += i + " ";
        }
        
        aisOdd = !aisOdd;
    }

    sum.value = result;
})


///// Task 2 /////

let numberOfShops = document.getElementById("number-of-shops");
let shipmentOptions = document.getElementById("shipment-options");

document.getElementById("calculate-options").addEventListener("click", function() {
    let result = 1;
    let i = 1;
    do
    {
        result *= i;
        ++i;
    }
    while (i <= Number(numberOfShops.value))
    shipmentOptions.value = result;
})

///// Task 3 /////

let rectangle = document.getElementById("rectangle");
let rigthTriangle = document.getElementById("right-triangle");
let equilateralTriangle = document.getElementById("equilateral-triangle");
let rhombus = document.getElementById("rhombus"); 
let space = "&nbsp&nbsp&nbsp&nbsp";
let halfSpace = "&nbsp&nbsp";

// rectangle
let sizeA = 5;
let sizeB = 7;
for (let i = 0; i < sizeA; ++i)
{
    rectangle.innerHTML += "<br>";
    for(let j = 0; j < sizeB; ++j)
    {
        rectangle.innerHTML += '*' + space;
    }
}
rectangle.innerHTML += "<hr>"

// right triangle
let size = 7;
for (let i = 0; i < size; ++i)
{
    rigthTriangle.innerHTML += "<br>";
    for(let j = i; j > 0; --j)
    {
        rigthTriangle.innerHTML += '*' + space;
    }
}
rigthTriangle.innerHTML += "<hr>"

// equilateral triangle
size = 8;
for (let i = 0; i < size; ++i)
{
    equilateralTriangle.innerHTML += "<br>";
    for(let j = (size - i) / 2; j > 0; --j)
    {
        equilateralTriangle.innerHTML += space + " ";
    }

    if(i % 2 === 0) equilateralTriangle.innerHTML += halfSpace;

    for(let j = i + 1; j > 0; --j)
    {
        equilateralTriangle.innerHTML += '*' + space;
    }
}
equilateralTriangle.innerHTML += "<hr>"

// rhombus
size = 8;
for (let i = 0; i < size; ++i)
{
    rhombus.innerHTML += "<br>";
    for(let j = (size - i) / 2; j > 0; --j)
    {
        rhombus.innerHTML += space + " ";
    }

    if(i % 2 === 0) rhombus.innerHTML += halfSpace;

    for(let j = i + 1; j > 0; --j)
    {
        rhombus.innerHTML += '*' + space;
    }
}

for (let i = size + 1; i > 0; --i)
{
    rhombus.innerHTML += "<br>" + halfSpace;
    for(let j = (size - i) / 2; j > 0; --j)
    {
        rhombus.innerHTML += space + " ";
    }

    if(i % 2 === 0) rhombus.innerHTML += halfSpace;

    for(let j = i; j > 0; --j)
    {
        rhombus.innerHTML += '*' + space;
    }
}
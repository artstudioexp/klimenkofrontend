///// Task 1 /////

let array = [];
let size = 0;
let arraySize = document.getElementById("array-size");
let arrayResult1 = document.getElementById("array-result1")
let arrayResult2 = document.getElementById("array-result2")
let arrayResult3 = document.getElementById("array-result3")
let arrayResult4 = document.getElementById("array-result4")
let oddNumbers = document.getElementById("odd-numbers");

document.getElementById("calculate-array").addEventListener("click", function() {
    console.log(size);
    let newSize = Number(arraySize.value);
    if(newSize === size) return;

    array = [];
    size = newSize;
    populateArray(size);
    let max = array[0];
    let min = array[0];
    let sum = 0;
    oddNumbers.textContent = "";
    for(num in array)
    {
        sum += array[num];
        if(min > array[num]) min = array[num];
        if(max < array[num]) max = array[num];
        if(array[num] % 2 !== 0) oddNumbers.textContent += array[num] + " ";
    }
    let average = (sum / size).toFixed(3);
    
    arrayResult1.innerText = max;
    arrayResult2.innerText = min;
    arrayResult3.innerText = sum;
    arrayResult4.innerText = average;
})

function populateArray(newSize)
{
    const maxInt = 50;
    const minInt = 0;
    for(let i = 0; i < newSize; ++i)
    {
        array[i] = getRandomInt(minInt, maxInt);
    }
}

function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min) + min);
}



///// Task 2 /////

let originalArrayDiv = document.getElementById("original-array");
let newArrayDiv = document.getElementById("new-array");

let originalArray = [];

// populate array
for(let i = 0; i < 5; ++i)
{
    originalArray.push([]);
    for(let j = 0; j < 5; ++j)
    {
        originalArray[i][j] = getRandomInt(-9, 9);
        originalArrayDiv.innerHTML += originalArray[i][j] + "&nbsp&nbsp";
    }
    originalArrayDiv.innerHTML += "<br>";
}


for(let i = 0; i < 5; ++i)
{
    // change main diagonal
    originalArray[i][i] = originalArray[i][i] > 0 ? 1 : 0;
    // wrire result
    for(let j = 0; j < 5; ++j)
    {
        newArrayDiv.innerHTML += originalArray[i][j] + "&nbsp&nbsp";
    }
    newArrayDiv.innerHTML += "<br>";
}


///// Task 3 //////

let vector;
let vectorInput = document.getElementById("vector");
let vectorResultInput = document.getElementById("new-vector")

document.getElementById("calculate-vector").addEventListener("click", function() {
    vector = vectorInput.value.split(" ");
    vectorResultInput.value = "";

    for(let i = 0; i < vector.length; ++i)
    {
        let average = 0;
        for(let j = 0; j < i; ++j)
        {
            average += Number(vector[j]);
        }

        if(i !== 0) vectorResultInput.value += average / i + " | ";
    }
})

///// Task 1 /////

let x = 6, y = 15, z = 4;

x += y - x++ * z;
printResult1(0);

z = -- x - y * 5;
printResult1(1);

y /= x + 5 % z;
printResult1(2);

z = x++ + y * 5;
printResult1(3);

x = y - x++ * z;
printResult1(4);

function printResult1(number)
{
    document.getElementById(`task1_${number}`).innerText = " Результат: " + "x = " + x + ", y = " + y + ", z = " + z + ".";
}


///// Task 2 /////

let x1 = document.getElementById("x1");
let x2 = document.getElementById("x2");
let x3 = document.getElementById("x3");
let Y = document.getElementById("y");
let calcAverButton = document.getElementById("calculate-average");

calcAverButton.addEventListener("click", function() {
    Y.value = ((Number(x1.value) + Number(x2.value) + Number(x3.value)) / 3.0).toFixed(3);
})


///// Task 3 /////

let h = document.getElementById("h");
let r = document.getElementById("r");
let S = document.getElementById("S");
let V = document.getElementById("V");
let calcVolButton = document.getElementById("calculate-volume");

calcVolButton.addEventListener("click", function() {
    let circ = 2.0 * Math.PI * Number(r.value);
    S.value = (circ * (Number(h.value) + Number(r.value))).toFixed(3);
    V.value = (Math.PI * Number(r.value) * Number(r.value) * Number(h.value)).toFixed(3);
})
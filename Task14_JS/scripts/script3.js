document.getElementById("execute").addEventListener("click", function() {
    const text = document.getElementById("text").value;
    const word = document.getElementById("word").value;
    let occurrences = findWordOccurences(word, text);
    jsConsole.writeLine("There " + (occurrences == 1? "is " : "are ") + occurrences + " occurrence" + (occurrences == 1? " " : "s ") + " of word \"" + word + "\" in text.");
    jsConsole.delimiter();
})

document.getElementById("clear").addEventListener("click", jsConsole.clear);

// calculate number of word occurrences in text
function findWordOccurences(word, text)
{
    word = word.toLowerCase();
    text = text.toLowerCase();
    let count = 0;
    let idx = text.indexOf(word);
    while(idx >= 0)
    {
        count++;
        idx = text.indexOf(word, idx + 1);
    }
    return count;
}
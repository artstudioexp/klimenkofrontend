document.getElementById("clear").addEventListener("click", jsConsole.clear);

document.getElementById("execute").addEventListener("click", function() {
    let text = document.getElementById("text").value;
    text = text.replace(/\s/g, "&nbsp");
    jsConsole.writeLineHTML(text);
    jsConsole.delimiter();
});
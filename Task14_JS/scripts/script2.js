document.getElementById("clear").addEventListener("click", jsConsole.clear);

document.getElementById("execute").addEventListener("click", function() {
    jsConsole.writeLine(EvaluateParenthesesExpression(document.getElementById("expression").value));
    jsConsole.delimiter();
});

function EvaluateParenthesesExpression(expression)
{
    let openingParentheses = 0;
    let closingParentheses = 0;
    let lastChar = '';
    for(i in expression)
    {
        switch(expression[i])
        {
            case '(':
                openingParentheses++;
                break;
            case ')':
                if(openingParentheses <= closingParentheses)
                {
                    return "Invalid Expression! Found closing parenthesis before the opening one. Position: " + i;
                }
                else if(lastChar === '(')
                {
                    return "Invalid Expression! Found empty parentheses. Position: " + i;
                }
                else
                {
                    closingParentheses++;
                }
                break;
            default:
                break;
        }
        lastChar = expression[i];
    }

    if(openingParentheses === closingParentheses)
        return "Valid Expression!";
    else
        return "Invalid Expression! Number of opening parentheses doesn't match the number of closing ones.";
}
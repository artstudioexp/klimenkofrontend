document.getElementById("clear").addEventListener("click", jsConsole.clear);

document.getElementById("execute").addEventListener("click", function() {
    jsConsole.writeLine(parseTags(document.getElementById("text").value));
    jsConsole.delimiter();
});

///////////////////////////////////////////////////////////
/// find tags in the text, alter text according to tags ///
///////////////////////////////////////////////////////////
function parseTags(text)
{
    let tagContentBegin = 0;
    let tagContentEnd = 0;
    let tag = "";
    let tagContent = "";
    let tagCount = 0;
    for(let i = 0; i < text.length; ++i)
    {
        //debugger;
        // find the beginning of the opening tag
        if(text[i] === '<')
        {
            // find the end of opening the tag
            let bracketIdx = text.indexOf('>', i);
            if(bracketIdx != -1)
            {
                // found 1 opening tag, increment tag count
                ++tagCount;
                tagContentBegin = bracketIdx + 1;
                // get tag name
                tag = text.substring(Number(i) + 1, bracketIdx);
                // find nearest tag with the same name
                let searchIdx = tagContentBegin;
                do
                {
                    searchIdx = text.indexOf(tag, searchIdx);
                    if(searchIdx === -1)
                    {
                        jsConsole.writeLine("Didn't find closing tag. Position: " + bracketIdx)
                        return -1;
                    }
                    // found tag is closing tag
                    if(text[searchIdx - 1] === '/' && text.slice(searchIdx - 2, searchIdx + tag.length + 1) === "</" + tag + '>')
                    {
                        --tagCount;
                        ++searchIdx;
                    }
                    // found tag isn't closing tag
                    else
                    {
                        ++tagCount;
                        ++searchIdx;
                    }
                }
                while(tagCount !== 0)

                tagContentEnd = searchIdx - 3;
                // extract tag content
                tagContent = text.slice(tagContentBegin, tagContentEnd);
                // alter tag content according to the tag
                tagContent = alterLine(tagContent, tag);
                // search through tag content for inside tags (recursion)
                tagContent = parseTags(tagContent);
                // cut tags from the initial text
                text = spliceString(text, tagContentBegin - tag.length - 2, tagContentEnd - tagContentBegin + 2 * tag.length + 5, tagContent);
                // move i forward to skip current tag content
                i += tagContent.length - 1;
            }
            else
            {
                jsConsole.writeLine("Parse error. Didn't find closing tag. Position: " + i);
                jsConsole.delimiter();
            }
        }

    }
    // no tags in the text
    return text;
}


////////////////////////////////////////////////////////////////////////////////////
///         Alter line according to a given tag, except for inside tags          ///
/// One can add any arbitrary tag to the switch block for it to work with parser ///
////////////////////////////////////////////////////////////////////////////////////
function alterLine(line, tag)
{
    let newLine = "";
    let insideTag = false;
    switch(tag)
    {
        case "upcase":
            for(i in line)
            {
                if(line[i] === '<')
                {
                    insideTag = true;
                }

                newLine += insideTag? line[i] : line[i].toUpperCase();

                if(line[i] === '>')
                {
                    insideTag = false;
                }
            }
            return newLine;
            break;

        case "lowcase":
            for(i in line)
            {
                if(line[i] === '<')
                {
                    insideTag = true;
                }

                newLine += insideTag? line[i] : line[i].toLowerCase();

                if(line[i] === '>')
                {
                    insideTag = false;
                }
            }
            return newLine;
            break;

        case "mixcase":
            for(i in line)
            {
                if(line[i] === '<')
                {
                    insideTag = true;
                }

                newLine += insideTag? line[i] : Math.random() > 0.5? line[i].toUpperCase() : line[i].toLowerCase();

                if(line[i] === '>')
                {
                    insideTag = false;
                }
            }
            return newLine;
            break;
            
        default:
            break;
    }
}

///////////////////////////////////////
/// works similarly to array.splice ///
///////////////////////////////////////
function spliceString(str, start, count, add)
{
    if(start < 0)
    {
        start = str.length + start;
        if(start < 0)
        {
            start = 0;
        }
    }
    return str.slice(0, start) + (add || "") + str.slice(start + count);
}



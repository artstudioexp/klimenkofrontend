document.getElementById("execute").addEventListener("click", function() {
    let text = document.getElementById("text").value;
    text = text.split("").reverse().join("");
    jsConsole.writeLine(text);
    jsConsole.delimiter();
});

document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", function() {
    let array = document.getElementById("array").value.split(',');
    let repeatedSequences = [];
    repeatedSequences.push([]);
    let count = 0;

    for (let i = 0; i < array.length; ++i)
    {
        if(i > 0 && array[i] === array[i - 1])
        {
            if(repeatedSequences[count][0] === array[i])
            {
                repeatedSequences[count].push(array[i]);
            }
            else
            {
                ++count;
                repeatedSequences.push([])
                repeatedSequences[count].push(array[i - 1], array[i]);
            }
        }
    }

    let maxIdx = 0;
    for (i in repeatedSequences)
    {
        if(repeatedSequences[i].length > repeatedSequences[maxIdx].length) maxIdx = i;
    }

    jsConsole.writeLine("The input sequence is:");
    jsConsole.writeLine(array);
    jsConsole.writeLine("");
    jsConsole.writeLine("The maximum sequence is:");
    jsConsole.writeLine(repeatedSequences[maxIdx]);
    jsConsole.delimiter();
})
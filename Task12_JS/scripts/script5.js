document.getElementById("execute").addEventListener("click", function() {
    let array = document.getElementById("array").value.split(',');
    let length = array.length;
    let sortedArray = []
    jsConsole.writeLine("Initial array:");
    jsConsole.writeLine(array);
    jsConsole.writeLine("");

    for(let i = 0; i < length; ++i)
    {
        let minIdx = 0;
        for(let j = 0; j < array.length; ++j)
        {
            if(Number(array[minIdx]) > Number(array[j])) minIdx = j;
        }
        console.log(minIdx);
        console.log(array);
        sortedArray.push(array.splice(minIdx, 1)[0]);
        console.log(array);
    }

    jsConsole.writeLine("Sorted array:")
    jsConsole.writeLine(sortedArray);
    jsConsole.delimiter();
})
document.getElementById("execute").addEventListener("click", function() {
    let array = document.getElementById("array").value.split(',');
    let searchedValue = parseInt(document.getElementById("value").value, 10);

    array.sort((a, b) => Number(a) > Number(b));

    jsConsole.writeLine("Entered array after sorting:");
    jsConsole.writeLine(array);
    jsConsole.writeLine("");
    jsConsole.writeLine("Number you've searched: " + searchedValue + ", index: " + binarySearch(searchedValue, array, 0, array.length));
    jsConsole.delimiter();
})

function binarySearch(value, array, idx1, idx2)
{
    let middleIdx = Math.floor((idx2 - idx1) / 2 + idx1);
    if(idx1 < 0 || idx2 < 0) return "There is no such number in the array";
    if(Number(value) > Number(array[middleIdx]))
    {
        return binarySearch(value, array, middleIdx + 1, idx2);
    }
    else if(Number(value) < Number(array[middleIdx]))
    {
        return binarySearch(value, array, idx1, middleIdx - 1);
    }
    else
    {
        if(middleIdx > array.length - 1) return "There is no such number in the array";
        return middleIdx;
    }
}
document.getElementById("execute").addEventListener("click", function() {
    let array = document.getElementById("array").value.split(',');
    let sequences = [];
    sequences.push([]);
    let count = 0;

    for (let i = 0; i < array.length; ++i)
    {
        if(i > 0 && (array[i] - array[i - 1]) === 1)
        {
            if(!sequences[count][0]) sequences[count].push(array[i - 1]);
            sequences[count].push(array[i]);
        }
        else
        {
            ++count;
            sequences.push([])
        }
    }

    let maxIdx = 0;
    for (i in sequences)
    {
        if(sequences[i].length > sequences[maxIdx].length) maxIdx = i;
    }

    jsConsole.writeLine("The input sequence is:");
    jsConsole.writeLine(array);
    jsConsole.writeLine("");
    jsConsole.writeLine("The maximum increasing sequence is:");
    jsConsole.writeLine(sequences[maxIdx]);
    jsConsole.delimiter();
})
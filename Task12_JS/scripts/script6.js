document.getElementById("execute").addEventListener("click", function() {
    let array = document.getElementById("array").value.split(',');
    let numbers = [];

    for(let i = 0; i < array.length; ++i)
    {
        if(!numbers[array[i]])
        {
            numbers[Number(array[i])] = 1;
            console.log(array[i]);
        }
        else
        {
            ++numbers[Number(array[i])];
        }
    }

    // get first non-empty index
    let maxIdx
    for(i in numbers)
    {
        maxIdx = i;
        break;
    }

    for(i in numbers)
    {
        if(Number(numbers[maxIdx]) < Number(numbers[i])) maxIdx = i;
        console.log("here");
    }
    console.log(numbers);
    jsConsole.writeLine("Entered array:");
    jsConsole.writeLine(array);
    jsConsole.writeLine("");
    jsConsole.writeLine("The most frequent number is " + maxIdx + ". Repeated " + numbers[maxIdx] + " times.");
    jsConsole.delimiter();
})
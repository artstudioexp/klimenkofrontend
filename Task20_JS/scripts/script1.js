document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);
document.getElementById("text").value = "This is the text containing email addresses like myemail@mail.ru. Different addresses have different hosts like gmail (hisemail@gmail.com) or hotmail (heremail@hotmail.com). Even VK has an email feature now: somebodysemail@vk.com"

function execute() 
{
    let text = document.getElementById("text").value;
    let addresses = text.match(/\b\w+\@\w+\.\w+\b/g);
    for(a of addresses) jsConsole.writeLine(a);
    jsConsole.delimiter(50, '-', "aquamarine");
}
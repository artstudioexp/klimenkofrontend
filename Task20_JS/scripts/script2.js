document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);
document.getElementById("text").value = "This is arbitrary text containing palindromes for function testing. Words like 'did', 'level', 'madam', 'racecar', etc. are called palindromes - they read the same backward or forward.";

function execute()
{
    let str = document.getElementById("text").value.replace(/[^\w]+/g, ' ');
    let words = str.split(" ").filter(w => w);
    for(w of words)
    {
        if(isPalindrome(w)) jsConsole.writeLine(w);
    }
    jsConsole.delimiter(50, '-', "aquamarine");
}

function isPalindrome(word)
{
    if(typeof(word) !== "string") return undefined;
    word = word.toLowerCase();
    return word === word.split("").reverse().join("");
}
document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);
document.getElementById("text").value = document.documentElement.innerHTML;

function execute() 
{
    let html = document.getElementById("text").value;
    let found = html.match(/(?<=>)([^<>]+)(?=<)/g);
    found = found.filter(n => n != 0);
    found = found.join("").replace(/\n+/g, "");
    jsConsole.writeLine(found);
    jsConsole.delimiter();
}
document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);

function execute()
{
    let result = parse(document.getElementById("url").value);
    jsConsole.writeLine(result);
}

function parse(str)
{
    let protocol = str.match(/^.+(?=:)/)[0];
    let server = str.match(/(?<=\/\/)[^\/]+(?=\/)/)[0];
    let resource = str.slice(protocol.length + server.length + 3);
    let info = {
        protocol: protocol,
        server: server,
        resource: resource
    }
    return JSON.stringify(info);
}
document.getElementById("clear").addEventListener("click", jsConsole.clear);
document.getElementById("execute").addEventListener("click", execute);
document.getElementById("text").value = '<p>Please visit <a href="http://academy.telerik. com">our site</a> to choose a training course. Also visit <a href="www.devbg.org">our forum</a> to discuss the courses.</p>';

function execute() 
{
    let str = document.getElementById("text").value;
    str = str.replace(/\<a href=/g, "[URL=");
    str = str.replace(/(?<=\[URL=".*")\>/g, "]");
    str = str.replace(/\<\/a\>/g, "[/URL]");
    jsConsole.writeLineHTML(str);
}